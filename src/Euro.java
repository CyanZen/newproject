public class Euro {

    private double value;

    public Euro(double value) {
        this.value = value;
    }

    public double getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        return ((Euro)o).value == value;
    }

    public Euro add(Euro euro) {
        return new Euro(this.value + euro.value);
    }
}
