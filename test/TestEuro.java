import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TestEuro {
    @Test
    public void shouldBeEqualValue() {
        Euro euro = new Euro(3.3);
        assertEquals(3.3, euro.getValue());
    }

    @Test
    void shouldBeNotTheSameId() {
        Euro euro = new Euro(10);
        Euro euro1 = new Euro(10);
        assertFalse(euro == euro1);
    }

    @Test
    void shouldBeEqual() {
        Euro euro = new Euro(10);
        Euro euro1 = new Euro(10);
        assertEquals(euro, euro1);
    }

    @Test
    void shouldNotBeEqual() {
        Euro euro = new Euro(5);
        Euro euro1 = new Euro(10);
        assertNotEquals(euro, euro1);
    }

    @Test
    void shouldNotEqualNull() {
        assertNotEquals(new Euro(10), null);
    }

    @Test
    void shouldNotEqualToInstanceOfAnotherClass() {
        assertNotEquals(new Euro(10), new Object());
    }

    @Test
    void shouldEqualToTheSum() {
        assertEquals(new Euro(10), new Euro(3).add(new Euro(7)));
    }

    @Test
    void shouldNotEqualToTheSum() {
        assertNotEquals(new Euro(10), new Euro(3).add(new Euro(5)));
    }
}